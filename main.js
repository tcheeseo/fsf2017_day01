//Load our libraries
var express = require("express"); // load lib and return an express object
var path    = require("path"); 

//console.info(">>> express: " + express);
//console.info(">>> path: " + path);

//Create an instance of the express application

var app = express();

//define routes, ordering is very impt!
app.use(express.static(__dirname + "/public")); // __dirname is the location node was started

app.use(function(req, resp) {
    resp.status(404);
    resp.type("text/html"); // representation
    resp.send("<h1>File not found</h1><p>The current time is " + new Date() + "</p>");
});


//console.info(">>> __dirname: " + __dirname);

// setting port as a property of the app
app.set("port", 3000); 

//console.info("Port: " + app.get("port"));

// start the server on the specified port
app.listen(app.get("port"), function() {
    console.info("Server started "+ app.get("port"));
});